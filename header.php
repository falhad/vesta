<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php wp_title(' | ', 'true', 'right'); ?><?php bloginfo('name'); ?></title>
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    <link href="<?php echo get_bloginfo('rss2_url'); ?>" title="<?php bloginfo('name'); ?>" type="application/rss+xml"
                                  rel="alternate">
    <?php wp_head(); ?>
</head>
<body>
<div class="wrapper">
    <header class="header">
        <div id="blog-title">
            <h1><a href="<?php echo home_url(); ?>"><?php bloginfo('name'); ?></a></h1>
            <h2><?php bloginfo('description'); ?></h2>
        </div>
        <div id="nav-wrapper">
            <section class="container">
                <nav>
                    <?php wp_nav_menu(array('menu' => 'primary')); ?>
                </nav>
            </section>
        </div>
    </header>